import React from "react";

export const themes = {
  light: {
    foreground: "#222222",
    background: "#e9e9e9",
  },
  dark: {
    foreground: "#e9e9e9",
    background: "#222222",
  },
};
export const ThemesContext = React.createContext(themes.dark);
